/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.Serializable;

/**
 *
 * @author Tncpop
 */
public class Friend implements  Serializable{
    private String name;
    private String tel;
    private int age;
    private int id;
    private static int lastId=1;

    public Friend(String name, int age,String tel ) {
        this.name = name;
        this.tel = tel;
        this.age = age;
        this.id = lastId++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws  Exception{
        if(age<0) 
            throw new Exception();
        this.age = age;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Friend{" + "name=" + name + ", tel=" + tel + ", age=" + age + ", id=" + id + '}';
    }
    
    public static void main(String[] args) {
        Friend f = new Friend("pom",70,"1234567");
        try{
            f.setAge(-1);
        
        } catch (Exception ex) {
            System.out.println("Age is lower than 0");
        }
        System.out.println(""+ f);
    }
}
